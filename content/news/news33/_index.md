---
title: "News #33について"
date: 2019-11-12
tags: [ タグ141, タグ47, info ]
---

## タグ

- タグ141
- タグ47
- info

## テスト文

> I will honour Christmas in my heart, and try to keep it all the year.  I
will live in the Past, the Present, and the Future.  The Spirits of all
Three shall strive within me.  I will not shut out the lessons that they
teach.  Oh, tell me that I may sponge away the writing on this stone!
		-- Charles Dickens

