---
title: "News #196について"
date: 2019-11-19
tags: [ タグ24, maintenance ]
---

## タグ

- タグ24
- maintenance

## テスト文

> Unless hours were cups of sack, and minutes capons, and clocks the tongues
of bawds, and dials the signs of leaping houses, and the blessed sun himself
a fair, hot wench in flame-colored taffeta, I see no reason why thou shouldst
be so superfluous to demand the time of the day.  I wasted time and now doth
time waste me.
		-- William Shakespeare

