---
title: "News #117について"
date: 2019-12-01
tags: [ タグ3, タグ139, info ]
---

## タグ

- タグ3
- タグ139
- info

## テスト文

> Q:	How many psychiatrists does it take to change a light bulb?
A:	Only one, but it takes a long time, and the light bulb has
	to really want to change.

