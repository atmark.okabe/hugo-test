---
title: "News #42について"
date: 2019-12-08
tags: [ タグ27, maintenance ]
---

## タグ

- タグ27
- maintenance

## テスト文

> Q:	How many existentialists does it take to screw in a light bulb?
A:	Two.  One to screw it in and one to observe how the light bulb
	itself symbolizes a single incandescent beacon of subjective
	reality in a netherworld of endless absurdity reaching out toward a
	maudlin cosmos of nothingness.

