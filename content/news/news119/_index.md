---
title: "News #119について"
date: 2019-11-22
tags: [ info ]
---

## タグ

- info

## テスト文

> It is easy to find fault, if one has that disposition.  There was once a man
who, not being able to find any other fault with his coal, complained that
there were too many prehistoric toads in it.
		-- Mark Twain, "Pudd'nhead Wilson's Calendar"

