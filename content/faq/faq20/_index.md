---
title: "Faq #20について"
date: 2019-12-15
tags: [ タグ42, タグ100, タグ74 ]
---

## タグ

- タグ42
- タグ100
- タグ74

## テスト文

> Q:	How do you shoot a blue elephant?
A:	With a blue-elephant gun.

Q:	How do you shoot a pink elephant?
A:	Twist its trunk until it turns blue, then shoot it with
	a blue-elephant gun.

