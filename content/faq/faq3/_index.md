---
title: "Faq #3について"
date: 2019-12-24
tags: [ タグ113 ]
---

## タグ

- タグ113

## テスト文

> There is no character, howsoever good and fine, but it can be destroyed by
ridicule, howsoever poor and witless.  Observe the ass, for instance: his
character is about perfect, he is the choicest spirit among all the humbler
animals, yet see what ridicule has brought him to.  Instead of feeling
complimented when we are called an ass, we are left in doubt.
		-- Mark Twain, "Pudd'nhead Wilson's Calendar"

