---
title: "Faq #47について"
date: 2019-12-26
tags: [ タグ181, タグ186 ]
---

## タグ

- タグ181
- タグ186

## テスト文

> Why is it that we rejoice at a birth and grieve at a funeral?  It is because we
are not the person involved.
		-- Mark Twain, "Pudd'nhead Wilson's Calendar"

